#!/bin/bash

rm sample-app.zip

zip sample-app.zip -r * --exclude '.git/*' --exclude 'node_modules/*' --exclude 'make-zip.sh'


#aws elasticbeanstalk --region "us-east-1" create-application-version --application-name "Cloudwatch sample application" --version-label "sample-app-v1" --source-bundle "S3Bucket=miquido-day10-cloudwatch-resources,S3Key=sample-app.zip"

