variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "key_name" {
  default = "Cloudwatch key"
}
variable "aws_region" {
  default = "us-east-1"
}

data "aws_caller_identity" "current" {}

locals {
  user_name = "${element(split("/", data.aws_caller_identity.current.arn), 1)}"
}


provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

data "aws_elastic_beanstalk_solution_stack" "nodejs" {
  most_recent = true

  name_regex = "^64bit Amazon Linux (.*) running Node.js$"
}

resource "aws_default_vpc" "default" {
}

# example ec2 instance

resource "aws_instance" "linux2" {
  ami           = "ami-035be7bafff33b6b6"
  instance_type = "t3.nano"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${data.aws_security_group.allow_ssh.id}"]
  iam_instance_profile = "${aws_iam_instance_profile.eb-profile.name}"

  tags = {
    Name = "ex2-${local.user_name}"
  }

  user_data = <<EOF
#!/bin/bash

echo 'while true; do
  :
done' > /cpu.sh
chmod u+x /cpu.sh

echo '
#!/bin/bash

/cpu.sh & PID=$!
sleep 10m
kill -9 $PID
' > /killhim.sh

chmod u+x /cpu.sh
chmod u+x /killhim.sh
/killhim.sh &

EOF

}

# example eb

resource "aws_iam_instance_profile" "eb-profile" {
  name = "eb-instance-${local.user_name}"
  role = "${aws_iam_role.eb-ec2-role.name}"
}

resource "aws_iam_role" "eb-ec2-role" {
  name = "eb-role-${local.user_name}"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach-01" {
  role       = "${aws_iam_role.eb-ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}
resource "aws_iam_role_policy_attachment" "test-attach-02" {
  role       = "${aws_iam_role.eb-ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}
resource "aws_iam_role_policy_attachment" "test-attach-03" {
  role       = "${aws_iam_role.eb-ec2-role.name}"
  policy_arn = "arn:aws:iam::588866844622:policy/EBCreateLogGroup"
}
resource "aws_iam_role_policy_attachment" "test-attach-04" {
  role       = "${aws_iam_role.eb-ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker"
}
resource "aws_iam_role_policy_attachment" "test-attach-05" {
  role       = "${aws_iam_role.eb-ec2-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
}

data "aws_elastic_beanstalk_application" "app" {
  name        = "Cloudwatch sample application"
}

data "aws_security_group" "allow_ssh" {
  name = "Allow ssh"
}

resource "aws_elastic_beanstalk_environment" "appenv" {
  name                = "${replace("${local.user_name}", ".", "-")}"
  application         = "${data.aws_elastic_beanstalk_application.app.name}"
  solution_stack_name = "${data.aws_elastic_beanstalk_solution_stack.nodejs.name}"
  version_label       = "sample-app-v1"

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "THEME"
    value     = "flatly"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "AWS_REGION"
    value     = "${var.aws_region}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "STARTUP_SIGNUP_TABLE"
    value     = "StartupSignupsTable"
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "NEW_SIGNUP_TOPIC"
    value     = "NewSignupTopic"
  }
  setting {
    namespace = "aws:elasticbeanstalk:container:nodejs"
    name      = "ProxyServer"
    value     = "nginx"
  }

  setting {
    namespace = "aws:elasticbeanstalk:container:nodejs:staticfiles"
    name      = "/static"
    value     = "/static"
  }

  #setting {
  #  namespace = "aws:elasticbeanstalk:environment"
  #  name      = "EnvironmentType"
  #  value     = "SingleInstance"
  #}

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${aws_iam_instance_profile.eb-profile.name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = "${var.key_name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${data.aws_security_group.allow_ssh.name}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "StreamLogs"
    value     = "true"
  }

  setting {
    namespace = "aws:elasticbeanstalk:cloudwatch:logs"
    name      = "DeleteOnTerminate"
    value     = "true"
  }
}

data "aws_instance" "appenv" {
  instance_id ="${element(aws_elastic_beanstalk_environment.appenv.instances, 1)}"
}

output "ex1" {
 value = "${format("ssh -i ./cloudwatch_key_rsa ec2-user@%s
  url: http://%s
  load balancer: %s",
  data.aws_instance.appenv.public_ip,
  aws_elastic_beanstalk_environment.appenv.cname,
  element(aws_elastic_beanstalk_environment.appenv.load_balancers, 1))
 }"
 description = "EB URL and ec2 instance with sample nodejs application"
}

output "ex2" {
 value = "${format("ssh -i ./cloudwatch_key_rsa ec2-user@%s", aws_instance.linux2.public_ip)}"
}

