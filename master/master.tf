variable "aws_access_key" {}
variable "aws_secret_key" {}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

# Key Pair
resource "aws_key_pair" "master_key" {
  key_name   = "Cloudwatch key"
  public_key = "${file("../cloudwatch_key_rsa.pub")}"
}

resource "aws_security_group" "allow_ssh" {
  name        = "Allow ssh"
  description = "Allow SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

}


# example eb

resource "aws_elastic_beanstalk_application" "app" {
  name        = "Cloudwatch sample application"
}

resource "aws_iam_policy" "policy" {
  name        = "CloudwatchLabsSampleAppPutItemFix"
  description = "Cloudwatch policy to let nodejs put items to dynamodb"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["dynamodb:PutItem"],
      "Effect": "Allow",
      "Resource": "arn:aws:dynamodb:*:*:table/StartupSignupsTable"
    }
  ]
}
EOF
}

